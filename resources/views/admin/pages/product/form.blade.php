@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Product</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($product)) Create @else Edit @endif Product</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form
                        @if(isset($product))
                        action="{{ route('admin.product.update', [$product->id]) }}"
                        @else
                        action="{{ route('admin.product.store') }}"
                        @endif
                        onsubmit="submitForm(this); return false;"
                        method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group ">
                                    <label for="title">Ten san pham</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Product name..."
                                           value="{{ old('name', $product->name ?? null)}}">
                                    <small class="text-danger rule"
                                           id="rule-name"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Product type</label>
                                    <input type="text" class="form-control" name="type" id="type"
                                           placeholder="Product type..."
                                           value="{{ old('type',  $product->type ?? null) }}">
                                    <small class="text-danger rule"
                                           id="rule-type"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="sub_type">Product sub type</label>
                                    <input type="text" class="form-control" name="sub_type" id="sub_type"
                                           placeholder="Product sub type..."
                                           value="{{ old('sub_type', $product->sub_type ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-sub_type"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="brand">Product Brand</label>
                                    <input type="text" class="form-control" name="brand" id="brand"
                                           placeholder="Product brand..."
                                           value="{{ old('brand', $product->brand ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-brand"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="industry">Product Industry</label>
                                    <input type="text" class="form-control" name="industry" id="industry"
                                           placeholder="Product industry..."
                                           value="{{ old('industry', $product->industry ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-industry"></small>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label for="category_ids">Danh Mục</label>
                                    <div class="select2-primary">
                                        <select class="form-control select2 select2-dropdown"
                                                name="category_ids[]"
                                                id="category_ids" multiple>
                                            @foreach($categories as $category)
                                                @if(isset($product))
                                                    <option
                                                        value="{{$category->_id }}"
                                                        {{ in_array($category->_id, old('category_ids', $product->category_ids ?? [])) ?"selected":null}}>
                                                        {{ $category->title}}
                                                    </option>
                                                @else
                                                    <option value="{{$category->_id}}"
                                                        {{ in_array($category->_id, old('category_ids', [])) ?"selected":null }}>
                                                        {{ $category->title }}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <small class="text-danger rule" id="rule-category_ids"></small>
                                </div>
                                <div class="form-group">
                                    <label>Trạng Thái</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($product->status))
                                            <option
                                                value="1" {{ old('status', $product->status) === "1" ? "selected" : null }}>
                                                Hiển thị
                                            </option>
                                            <option
                                                value="0" {{ old('status', $product->status) === "0" ? "selected" : null }}>
                                                Ẩn
                                            </option>
                                        @else
                                            <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển thị
                                            </option>
                                            <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                            </option>
                                        @endif
                                    </select>

                                    <small class="text-danger rule"
                                           id="rule-status"></small>
                                </div>
                                <div class="form-group">
                                    <label for="name">Thumbnail</label>
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a data-input="thumbnail" data-preview="holder" class="lfm btn btn-primary">
                                           <i class="fa fa-picture-o"></i> Choose
                                         </a>
                                       </span>
                                        <input id="thumbnail" class="form-control" type="text" name="thumbnail"
                                               value="{{ isset($product->image) ? $product->image : old('image')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="slide-container">
                                <label for="slide">slide</label>
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <a data-input="slide" data-preview="holder"
                                               class="lfm-multi btn btn-primary">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                        </span>
                                    <input id="slide" class="form-control" type="text" hidden>

                                </div>
                            </div>
                            <div id="slider_container" class="row"></div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-submit">@if(isset($product))
                                    Sửa @else Tạo @endif</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();
        appendSlide();

        function appendSlide() {
            let index = getIndex();
            let html = `
`
            $('#slide-container').append(html);
        }

        function getIndex() {
            let element = document.getElementById("slide-container");
            return element.getElementsByTagName("input").length;
        }
    </script>
@endsection

