@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Đơn Đặt Hàng</h1>
                            <br>
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p>Có tất cả {{ $orders->total() }} kết quả</p>
                                            <table id="example2"
                                                   class="table table-striped"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th style="width: 10px">#</th>
                                                    <th>Tên</th>
                                                    <th>Email</th>
                                                    <th>Số điện thoại</th>
                                                    <th>Theme</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($orders as $order)
                                                    <tr class="even">
                                                        <td>
                                                            #{{ ($orders->currentPage()-1) * $orders->perPage() + $loop->iteration }}</td>
                                                        <td>
                                                            {{ $order->name ?? "" }}
                                                        </td>
                                                        <td>{{ $order->email ?? "" }}</td>
                                                        <td>{{ $order->phone ?? "" }}</td>
                                                        <td>{{ $order->address ?? ""}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        {{ $orders->withPath(url()->current())->appends(request()->query())->links() }}
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>

@endsection

