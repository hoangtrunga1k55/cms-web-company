@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Quyền</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($permission)) Sửa @else Tạo @endif Quyền</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form @if(isset($permission))
                          action="{{ route('admin.permission.update', [$permission->id]) }}"
                          @else
                          action="{{ route('admin.permission.store') }}"
                          @endif
                          onsubmit="submitForm(this); return false;"
                          method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-12 col-xl-6">
                                <div class="form-group ">
                                    <label for="name">Tên</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Nhập tên" value="{{ $permission->name ?? null}}">
                                    <small class="text-danger rule"
                                           id="rule-name"></small>
                                </div>
                            </div>
                            <div class="col-12 col-xl-6">
                                <div class="form-group">
                                    <label for="group">Nhóm</label>
                                    <input type="text" class="form-control" name="group" id="group"
                                           placeholder="Nhập tên nhóm" value="{{ $permission->group ?? null}}">
                                    <small class="text-danger rule"
                                           id="rule-group"></small>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="routes">Routes</label>
                                    <select class="duallistbox" multiple="multiple" name="routes[]" id="routes">
                                        @if(isset($permission))
                                            @foreach($filteredRoutes as $route)
                                                <option
                                                    value="{{ $route }}" {{ in_array($route, $permission->routes) ? "selected" : null }}>{{ $route }}</option>
                                            @endforeach
                                        @else
                                            @foreach($filteredRoutes as $route)
                                                <option value="{{ $route }}">{{ $route }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                                <!-- /.form-group -->
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.css') }}">
    <script src="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.js') }}"></script>
    <script>
        $('.duallistbox').bootstrapDualListbox();
    </script>
@endsection
