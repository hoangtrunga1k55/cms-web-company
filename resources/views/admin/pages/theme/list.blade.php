@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Giao Diện</h1>
                            <br>
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    {{--                                    <form class="row">--}}
                                    {{--                                        <div class="col-6 col-xl-3">--}}
                                    {{--                                            <div class="form-group ">--}}
                                    {{--                                                <label>Tiêu đề</label>--}}
                                    {{--                                                <input type="text" class="form-control" name="title" id="title"--}}
                                    {{--                                                       placeholder="Nhập tiêu đề" value="{{$request->title}}">--}}
                                    {{--                                            </div>--}}
                                    {{--                                            <div class="form-group ">--}}
                                    {{--                                                <label for="name">Tên người viết</label>--}}
                                    {{--                                                <input type="text" class="form-control" name="name" id="name"--}}
                                    {{--                                                       placeholder="Nhập tên người viết" value="{{$request->name}}">--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-12">--}}
                                    {{--                                            <button type="submit" class="btn btn-success">Tìm kiếm--}}
                                    {{--                                            </button>--}}
                                    {{--                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </form>--}}
                                    <div class="row {{-- pt-5 --}}">
                                        <div class="col-sm-12">
                                            <p>Có tất cả {{ $themes->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                            <table id="example2"
                                                   class="table table-striped"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th>#</th>
                                                    <th>Tên giao diện</th>
                                                    <th>Giá</th>
                                                    <th>Trạng Thái</th>
                                                    <th>Hành Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @forelse($themes as $theme)
                                                    <tr>
                                                        <td>
                                                            #{{ ($themes->currentPage()-1) * $themes->perPage() + $loop->iteration }}</td>
                                                        <td>
                                                            {{$theme->title ?? ""}}
                                                        </td>
                                                        <td>
                                                            {{ $theme->price ?? "" }} {{ $theme->unit ?? "" }}
                                                        </td>
                                                        <td>
                                                            {{ $theme->getStatus() }}
                                                        </td>
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    theme-id="{{ $theme->id }}">
                                                                <option readonly>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.new.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa bài viết
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.new.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa bài viết
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @empty
                                                    <tr>
                                                        <td colspan="6">Không tồn tại bản ghi nào</td>
                                                    </tr>
                                                @endforelse

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $themes->withPath(url()->current())->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection

@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('theme-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.theme.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.theme.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }

    </script>
@endsection

