@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Giao diện</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form
                            @if(isset($theme))
                            action="{{ route('admin.theme.update', [$theme->id]) }}"
                            @else
                            action="{{ route('admin.theme.store') }}"
                            @endif
                            onsubmit="submitForm(this); return false;"
                            method="post" enctype="multipart/form-data"
                            class="card card-primary card-outline card-outline-tabs">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#custom-tabs-four-home" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Thông
                                            tin chung</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill"
                                           href="#custom-tabs-four-profile" role="tab"
                                           aria-controls="custom-tabs-four-profile" aria-selected="false"
                                           style="">SEO</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        <div class="row">
                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="title">Tên giao diện</label>
                                                        <input type="text" class="form-control" name="title" id="title"
                                                               value="{{ old('title', $theme->title ?? "") }}"
                                                               placeholder="Nhập Tên giao diện">
                                                        <small class="text-danger rule" id="rule-title"></small>
                                                    </div>
                                                    <div class="form-group col-12  col-lg-6">
                                                        <label for="price">Giá</label><br>
                                                        <input type="number" min="0" class="form-control" name="price"
                                                               id="price"
                                                               value="{{ old('price', $theme->price ?? "") }}"
                                                               placeholder="Nhập Giá giao diện">
                                                        <small class="text-danger rule" id="rule-price"></small>
                                                    </div>
                                                    <div class="form-group col-12  col-lg-6">
                                                        <label for="unit">Đơn vị tiền tệ</label><br>
                                                        <input type="text" class="form-control" name="unit" id="unit"
                                                               value="{{ old('unit', $theme->unit ?? "") }}"
                                                               placeholder="Nhập Đơn vị tiền tệ">
                                                        <small class="text-danger rule" id="rule-unit"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="completion_date">Thời gian hoàn thành (Ngày)</label><br>
                                                        <input type="number" min="0" class="form-control"
                                                               name="completion_date" id="completion_date"
                                                               value="{{ old('name', $theme->completion_date ?? "") }}"
                                                               placeholder="Nhập Thời gian hoàn thành">
                                                        <small class="text-danger rule"
                                                               id="rule-completion_date"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="status">Chi tiết thiết kế</label><br>
                                                        <small class="text-danger rule" id="rule-content"></small>
                                                        <textarea name="content"
                                                                  id="content">{{ old('content', $theme->content ?? "") }}</textarea>
                                                    </div>
                                                    <script>
                                                        let editor = CKEDITOR.replace('content');
                                                        CKEDITOR.config.extraPlugins = "toc";
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label>Ngôn ngữ</label>
                                                        <select class="form-control" id="lang_type" name="lang_type">
                                                            @foreach($languages as $language)
                                                                @if($language['code'] != 'all')
                                                                    <option
                                                                        value="{{$language->code}}" {{ old('lang_type', isset($theme->lang_type) ? $theme->lang_type ?? "" : "") == $language->code  ? "selected" : null }}>
                                                                        {{$language->name}}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <small class="text-danger rule" id="rule-lang_type"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label>Trạng Thái</label>
                                                        <select class="form-control" id="status" name="status">
                                                            @foreach(\App\Models\Theme::STATUS as $value => $status)
                                                                <option
                                                                    value="{{ $value }}" {{ old('status', !isset($theme) ? $theme->status ?? "" : "") === $value ? "selected" : null }}>
                                                                    {{ $status }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        <small class="text-danger rule" id="rule-status"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="demo_web">Link Demo (Web)</label>
                                                        <input type="text" class="form-control" name="demo_web"
                                                               id="demo_web"
                                                               value="{{ old('demo_web', $theme->demo_web ?? "") }}">
                                                        <small class="text-danger rule" id="rule-demo_web"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="demo_android">Link Demo (Android)</label>
                                                        <input type="text" class="form-control" name="demo_android"
                                                               id="demo_android"
                                                               value="{{ old('demo_android', $theme->demo_android ?? "") }}">
                                                        <small class="text-danger rule" id="rule-demo_android"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="demo_ios">Link Demo (iOS)</label>
                                                        <input type="text" class="form-control" name="demo_ios"
                                                               id="demo_ios"
                                                               value="{{ old('demo_ios', $theme->demo_ios ?? "") }}">
                                                        <small class="text-danger rule" id="rule-demo_ios"></small>
                                                    </div>

                                                    <div class="form-group col-12">
                                                        <label for="name">Ảnh đại diện</label>
                                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder"
                                                                class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                            <input id="image" class="form-control" type="text"
                                                                   name="image"
                                                                   value="{{ isset($theme->image) ? $theme->image : old('image')}}">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-image"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-profile-tab">
                                        <div class="row">

                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="meta_title">Meta Title</label>
                                                        <input type="text" class="form-control" name="meta_title"
                                                               id="meta_title" maxlength="70"
                                                               value="{{ old('meta_title', $theme->meta_title ?? "") }}"
                                                               placeholder="Nhập Meta Title">
                                                        <small class="text-danger rule" id="rule-meta_title"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_description">Meta Description</label>
                                                        <input type="text" class="form-control" name="meta_description"
                                                               id="meta_description" maxlength="160"
                                                               value="{{ old('meta_description', $theme->meta_description ?? "") }}"
                                                               placeholder="Nhập Meta Description">
                                                        <small class="text-danger rule"
                                                               id="rule-meta_description"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_keywords">Meta Keywords</label>
                                                        <input type="text" class="form-control" name="meta_keywords"
                                                               id="meta_keywords" maxlength="160"
                                                               value="{{ old('meta_keywords', $theme->meta_keywords ?? "") }}"
                                                               placeholder="Nhập Meta Keywords">
                                                        <small class="text-danger rule" id="rule-meta_keywords"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="name">Meta Thumbnail</label>
                                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder"
                                                                class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                            <input id="image" class="form-control" type="text"
                                                                   name="meta_thumbnail"
                                                                   value="{{ isset($theme->meta_thumbnail) ? $theme->meta_thumbnail : old('meta_thumbnail')}}">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-meta_thumbnail"></small>
                                                    <img id="src_meta_thumbnail"
                                                         src="{{ old('meta_thumbnail', $theme->meta_thumbnail ?? "") }}"
                                                         alt="" class="img-thumbnail">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">
                                    @if(!isset($theme)) Thêm mới @else Cập nhật @endif
                                </button>
                                @if(isset($theme))
                                    @if( \App\Helpers\PermissionsHelper::can('admin.new.approve'))
                                        <button class="btn btn-success btn-modal" news-id="{{ $theme->id }}"
                                                onclick="changeStatus(this)"
                                                value="{{ \App\Models\News::WAIT_RELEASE }}">
                                            Duyệt bài viết
                                        </button>
                                    @endif
                                    @if( \App\Helpers\PermissionsHelper::can('admin.new.publish'))
                                        <button class="btn btn-success btn-modal" news-id="{{ $theme->id }}"
                                                onclick="changeStatus(this)"
                                                value="{{ \App\Models\News::RELEASE }}">
                                            Công bố bài viết
                                        </button>
                                    @endif
                                @endif
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();

        function changeStatus(event) {
            $(event).prop('disabled', true);
            console.log(event.getAttribute('news-id'), event.value)
            $.ajax({
                method: "POST",
                url: "{{ route('admin.new.update-status') }}",
                data: {
                    id: event.getAttribute('news-id'),
                    status: event.value,
                },
                success: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: data.status,
                        title: data.message
                    }).then(function () {
                        location.reload();
                    });
                },
                error: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: "error",
                        title: data.responseJSON.message
                    });
                }
            })
        }
    </script>
    <script>
        $("select.tag ").select2({
            tags: true,
        })
    </script>
@endsection
