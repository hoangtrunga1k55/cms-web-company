@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tạo gói bán hàng</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($package)) Sửa @else Tạo @endif gói bán hàng</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form @if(isset($package))
                          action="{{ route('admin.package.update', [$package->id]) }}"
                          @else
                          action="{{ route('admin.package.store') }}"
                          @endif
                          onsubmit="submitForm(this); return false;"
                          method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group ">
                                    <label for="title">Tên Danh Mục</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Nhập Tên" value="{{ old('name', $package->name ?? "") }}">
                                </div>
                                <small class="text-danger rule"
                                       id="rule-name"></small>
                                <div class="form-group ">
                                    <label for="title">Mô tả ngắn</label>
                                    <input type="text" class="form-control" name="desc" id="desc"
                                           placeholder="Nhập Mô tả ngắn"
                                           value="{{ old('desc', $package->desc ?? "") }}">
                                </div>
                                <small class="text-danger rule"
                                       id="rule-desc"></small>
                                <div class="form-group ">
                                    <label for="title">Link</label>
                                    <input type="text" class="form-control" name="link" id="link"
                                           placeholder="Nhập Link" value="{{ old('link', $package->link ?? '') }}">
                                </div>
                                <small class="text-danger rule"
                                       id="rule-link"></small>
                                <div class="form-group ">
                                    <label for="title">Độ Ưu Tiên</label>
                                    <input type="text" class="form-control" name="priority" id="priority"
                                           placeholder="Nhập độ ưu tiên"
                                           value="{{ old('priority', $package->priority ?? "") }}">
                                </div>
                                <small class="text-danger rule"
                                       id="rule-priority"></small>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Trạng Thái</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($package->status))
                                            <option
                                                value="1" {{ old('status', $package->status) === "1" ? "selected" : null }}>
                                                Hiển
                                                thị
                                            </option>
                                            <option
                                                value="0" {{ old('status', $package->status) === "0" ? "selected" : null }}>
                                                Ẩn
                                            </option>
                                        @else
                                            <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển thị
                                            </option>
                                            <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                            </option>
                                        @endif
                                    </select>
                                </div>
                                <small class="text-danger rule"
                                       id="rule-status"></small>
                                <div class="form-group ">
                                    <label>Nhập Mặt Hàng</label>
                                    <br>
                                    <textarea class="form-control" rows="9" name="titles" id="titles"
                                              placeholder="Mặt hàng ngăn cách bằng dấu ,">{{ old('titles', $package->titles ?? null) }}</textarea>

                                </div>
                                <small class="text-danger rule"
                                       id="rule-titles"></small>

                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">@if(isset($package)) Sửa @else
                                    Tạo @endif</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
