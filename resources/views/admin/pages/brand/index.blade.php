@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Đối tác</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $brands->total() }} kết quả</p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tiêu đề</th>
                                    <th style="width: 150px">Ảnh</th>
                                    <th style="width: 150px">Trạng Thái</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($brands as $brand)
                                    <tr>
                                        <td>
                                            #{{ ($brands->currentPage()-1) * $brands->perPage() + $loop->iteration }}</td>
                                        <td>{{ $brand->name ?? ""}}</td>
                                        <td><img src="{{ $brand->image }}"
                                                 class="img-thumbnail" alt=""></td>
                                        @if($brand->status ==1)
                                            <td>Có</td>
                                        @else
                                            <td>Không</td>
                                        @endif
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    brand-id="{{ $brand->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.brand.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa đối tác
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.brand.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa đối tác
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $brands->appends(request()->query())->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('brand-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.brand.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.brand.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
