@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Đối tác</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($brand)) Sửa @else Tạo @endif đối tác</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form @if(isset($brand))
                          action="{{ route('admin.brand.update', [$brand->id]) }}"
                          @else
                          action="{{ route('admin.brand.store') }}"
                          @endif
                          onsubmit="submitForm(this); return false;"
                          enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-lg-3 col-sm-12">
                                <div class="form-group ">
                                    <label for="name">Nhập tên đối tác</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Nhập tên đối tác" value="{{ $brand->name ?? null}}">

                                    <small class="text-danger rule"
                                           id="rule-name"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Mô tả ngắn</label>
                                    <input type="text" class="form-control" name="description" id="description"
                                           placeholder="Nhập Mô tả ngắn" value="{{ $brand->description ?? null}}">

                                    <small class="text-danger rule"
                                           id="rule-description"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Link</label>
                                    <input type="text" class="form-control" name="link" id="link"
                                           placeholder="Nhập Link" value="{{ $brand->link ?? null}}">

                                    <small class="text-danger rule"
                                           id="rule-link"></small>
                                </div>
                            </div>

                            <div class="col-lg-3 col-sm-12">
                                <div class="form-group">
                                    <label for="status">Trạng thái</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($brand->status))
                                            <option
                                                value="1" {{ old('status', $brand->status) === "1" ? "selected" : null }}>
                                                Hiển thị
                                            </option>
                                            <option
                                                value="0" {{ old('status', $brand->status) === "0" ? "selected" : null }}>
                                                Ẩn
                                            </option>
                                        @else
                                            <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển thị
                                            </option>
                                            <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                            </option>
                                        @endif
                                    </select>

                                    <small class="text-danger rule"
                                           id="rule-status"></small>
                                </div>
                                <div class="form-group">
                                    <label for="name">Hình Ảnh</label>
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                           <i class="fa fa-picture-o"></i> Choose
                                         </a>
                                       </span>
                                        <input id="image" class="form-control" type="text" name="image"
                                               value="{{ old('image', $brand->image ?? "") }}">
                                    </div>
                                </div>
                                <small class="text-danger rule" id="rule-image"></small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
