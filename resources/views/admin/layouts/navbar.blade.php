<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('admin.dashboard')}}" class="nav-link">Trang Chủ</a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto" style="position: relative">
        <!-- Language Dropdown Menu -->
        <li class="nav-item dropdown show" style="position: fixed;right: 0px;top: 15px">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
                <i class="fa fa-language" style="font-size: 21px" aria-hidden="true"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right p-0" style="left: inherit; right: 0px;">
                @if(isset($languages))
                    @foreach($languages as $language)
                        @if($language['code']!= 'all')
                        <a href="{{route('language',$language['code'])}}"
                           class="dropdown-item {{((\Illuminate\Support\Facades\Session::get('language')==$language['code'] )?'active':'')}}">
                            <i class="flag-icon flag-icon-us mr-2"></i> {{$language['name']}}
                        </a>
                        @endif
                    @endforeach
                @else
                    <a href="{{route('language','vn')}}" class="dropdown-item">
                        <i class="flag-icon flag-icon-us mr-2"></i> VietNam
                    </a>
                    <a href="{{route('language','en')}}" class="dropdown-item">
                        <i class="flag-icon flag-icon-us mr-2"></i> English
                    </a>
                @endif
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
