@extends('auth.layouts.master')
@section('content')
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ $message }}</p>
        </div>
        <!-- /.login-card-body -->
    </div>
@endsection
