<?php

use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\SEOController;
use App\Http\Controllers\Admin\ThemeController;
use App\Http\Controllers\Admin\VendorController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\NewController;
use App\Http\Controllers\Admin\ConfigController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\HeaderController;
use App\Http\Controllers\Admin\MemberController;
use App\Http\Controllers\Admin\LanguageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'namespace' => 'Auth'
], function () {
    Route::get('login', 'LoginController@loginForm')->name('login');
    Route::post('login', 'LoginController@handleLogin');

    Route::get('logout', 'LogoutController@logOut')->name('logout');

    Route::get('forgot-password', 'ForgotPasswordController@forgotPasswordForm')->name('forgot_password');
    Route::post('forgot-password', 'ForgotPasswordController@handleForgotPasswordRequest');

    Route::get('reset-password', 'ResetPasswordController@formResetPassword')->name('reset_password');
    Route::post('reset-password', 'ResetPasswordController@handleResetPasswordRequest');

    Route::get('login/google', 'LoginController@redirectToProvider')->name('login.google');
    Route::get('login/google/callback', 'LoginController@handleProviderCallback');

});

Route::group([
    'prefix' => 'cp-admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'auth_logs','locale']
], function () {
    Route::group([
        'prefix' => 'profile'
    ], function () {
        Route::get('reset', [NewController::class, 'resetDatetime']);
        Route::get('/', [ProfileController::class, 'index'])->name('admin.profile.index');
        Route::post('/update-info', [ProfileController::class, 'updateInfo'])->name('admin.profile.update_info');
        Route::post('/update-password', [ProfileController::class, 'updatePassword'])->name('admin.profile.update_password');
    });
});
Route::group([
    'prefix' => 'cp-admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'permission','auth_logs','locale']
], function () {
    Route::get('/', 'DashboardController@index')->name('admin.dashboard');
    // Route Cms Accountdash
    Route::group([
        'prefix' => 'cms-accounts'
    ], function () {
        Route::get('/', 'CmsAccountController@index')->name('admin.cms-account.index');
        Route::get('/create', 'CmsAccountController@create')->name('admin.cms-account.create');
        Route::post('/create', 'CmsAccountController@store')->name('admin.cms-account.store');
        Route::get('/{id}/edit', 'CmsAccountController@edit')->name('admin.cms-account.edit');
        Route::post('/{id}/edit', 'CmsAccountController@update')->name('admin.cms-account.update');
        Route::delete('/{id}/destroy/', 'CmsAccountController@destroy')->name('admin.cms-account.destroy');
    });

    Route::group([
        'prefix' => 'member'
    ], function () {
        Route::get('/', 'MemberController@index')->name('admin.member.index');
        Route::get('/{id}/edit', 'MemberController@edit')->name('admin.member.edit');
        Route::post('/{id}/edit', 'MemberController@update')->name('admin.member.update');
        Route::delete('/{id}/destroy/', 'MemberController@destroy')->name('admin.member.destroy');
    });
    // Route Category
    Route::group([
        'prefix' => 'category'
    ], function () {
        Route::get('/', [CategoryController::class, 'index'])->name('admin.category.index');
        Route::get('/create', [CategoryController::class, 'create'])->name('admin.category.create');
        Route::post('/create', [CategoryController::class, 'store'])->name('admin.category.store');
        Route::get('/{id}/edit', [CategoryController::class, 'edit'])->name('admin.category.edit');
        Route::post('/{id}/edit', [CategoryController::class, 'update'])->name('admin.category.update');
        Route::delete('/{id}/destroy/', [CategoryController::class, 'destroy'])->name('admin.category.destroy');
    });
    // Route Category
    Route::group([
        'prefix' => 'news'
    ], function () {
        Route::get('/', [NewController::class, 'index'])->name('admin.new.index');
        Route::get('/create', [NewController::class, 'create'])->name('admin.new.create');
        Route::post('/create', [NewController::class, 'store'])->name('admin.new.store');
        Route::get('/show', [NewController::class, 'show'])->name('admin.new.show');
        Route::get('/{id}/edit', [NewController::class, 'edit'])->name('admin.new.edit');
        Route::post('/{id}/edit', [NewController::class, 'update'])->name('admin.new.update');
        Route::delete('/{id}/destroy/', [NewController::class, 'destroy'])->name('admin.new.destroy');
        Route::get('/{id}/approve', [NewController::class, 'approve'])->name('admin.new.approve');
        Route::get('/{id}/publish', [NewController::class, 'publish'])->name('admin.new.publish');
        Route::post('/update-status', [NewController::class, 'updateStatus'])->name('admin.new.update-status');

    });
    Route::group([
        'prefix' => 'tag'
    ], function () {
        Route::get('/', [TagController::class, 'index'])->name('admin.tag.index');
        Route::get('/create', [TagController::class, 'create'])->name('admin.tag.create');
        Route::post('/create', [TagController::class, 'store'])->name('admin.tag.store');
        Route::get('/{id}/edit', [TagController::class, 'edit'])->name('admin.tag.edit');
        Route::post('/{id}/edit', [TagController::class, 'update'])->name('admin.tag.update');
        Route::delete('/{id}/destroy/', [TagController::class, 'destroy'])->name('admin.tag.destroy');
    });
    Route::group([
        'prefix' => 'vendor'
    ], function () {
        Route::get('/', [VendorController::class, 'index'])->name('admin.vendor.index');
        Route::get('/create', [VendorController::class, 'create'])->name('admin.vendor.create');
        Route::post('/create', [VendorController::class, 'store'])->name('admin.vendor.store');
        Route::get('/{id}/edit', [VendorController::class, 'edit'])->name('admin.vendor.edit');
        Route::post('/{id}/edit', [VendorController::class, 'update'])->name('admin.vendor.update');
        Route::delete('/{id}/destroy/', [VendorController::class, 'destroy'])->name('admin.vendor.destroy');
    });

    Route::group([
        'prefix' => 'language'
    ], function () {
        Route::get('/config_language', [LanguageController::class, 'getLang'])->name('language_config');
        Route::post('/config_language/save', [LanguageController::class,'save'])->name('language_config.save');
        Route::get('/', [LanguageController::class, 'index'])->name('admin.language.index');
        Route::get('/create', [LanguageController::class, 'create'])->name('admin.language.create');
        Route::post('/create', [LanguageController::class, 'store'])->name('admin.language.store');
        Route::get('/{id}/edit', [LanguageController::class, 'edit'])->name('admin.language.edit');
        Route::post('/{id}/edit', [LanguageController::class, 'update'])->name('admin.language.update');
        Route::delete('/{id}/destroy/', [LanguageController::class, 'destroy'])->name('admin.language.destroy');
    });

    Route::group([
        'prefix' => 'config'
    ], function () {
        Route::get('/', [ConfigController::class, 'index'])->name('admin.config.index');
        Route::get('/create', [ConfigController::class, 'create'])->name('admin.config.create');
        Route::post('/create', [ConfigController::class, 'store'])->name('admin.config.store');
        Route::get('/{id}/edit', [ConfigController::class, 'edit'])->name('admin.config.edit');
        Route::post('/{id}/edit', [ConfigController::class, 'update'])->name('admin.config.update');
        Route::delete('/{id}/destroy/', [ConfigController::class, 'destroy'])->name('admin.config.destroy');
    });
    //contact
    Route::group([
        'prefix' => 'contact'
    ], function () {
        Route::get('/', [ContactController::class, 'index'])->name('admin.contact.index');
        Route::post('/', [ContactController::class, 'getInfo'])->name('admin.contact.store');
    });
    //Banner
    Route::group([
        'prefix' => 'banner'
    ], function () {
        Route::get('/', 'BannerController@index')->name('admin.banner.index');
        Route::get('/create', 'BannerController@create')->name('admin.banner.create');
        Route::post('/create', 'BannerController@store')->name('admin.banner.store');
        Route::get('/{id}/edit', 'BannerController@edit')->name('admin.banner.edit');
        Route::post('/{id}/edit', 'BannerController@update')->name('admin.banner.update');
        Route::delete('/{id}/destroy/', 'BannerController@destroy')->name('admin.banner.destroy');
    });

    Route::group([
        'prefix' => 'package'
    ], function () {
        Route::get('/', 'PackageController@index')->name('admin.package.index');
        Route::get('/create', 'PackageController@create')->name('admin.package.create');
        Route::post('/create', 'PackageController@store')->name('admin.package.store');
        Route::get('/{id}/edit', 'PackageController@edit')->name('admin.package.edit');
        Route::post('/{id}/edit', 'PackageController@update')->name('admin.package.update');
        Route::delete('/{id}/destroy/', 'PackageController@destroy')->name('admin.package.destroy');
    });

    //Đối tác
    Route::group([
        'prefix' => 'brand'
    ], function () {
        Route::get('/', 'BrandController@index')->name('admin.brand.index');
        Route::get('/create', 'BrandController@create')->name('admin.brand.create');
        Route::post('/create', 'BrandController@store')->name('admin.brand.store');
        Route::get('/{id}/edit', 'BrandController@edit')->name('admin.brand.edit');
        Route::post('/{id}/edit', 'BrandController@update')->name('admin.brand.update');
        Route::delete('/{id}/destroy/', 'BrandController@destroy')->name('admin.brand.destroy');
    });


    Route::group([
        'prefix' => 'permissions'
    ], function () {
        Route::get('/', [PermissionController::class, 'index'])
            ->name('admin.permission.index');
        Route::get('/create', [PermissionController::class, 'create'])
            ->name('admin.permission.create');
        Route::post('/create', [PermissionController::class, 'store'])
            ->name('admin.permission.store');
        Route::get('/{id}/edit', [PermissionController::class, 'edit'])
            ->name('admin.permission.edit');
        Route::post('/{id}/edit', [PermissionController::class, 'update'])
            ->name('admin.permission.update');
        Route::delete('/{id}/destroy/', [PermissionController::class, 'destroy'])
            ->name('admin.permission.destroy');
    });
    //Route Role
    Route::group([
        'prefix' => 'roles'
    ], function () {
        Route::get('/', [RoleController::class, 'index'])
            ->name('admin.role.index');
        Route::get('/create', [RoleController::class, 'create'])
            ->name('admin.role.create');
        Route::post('/create', [RoleController::class, 'store'])
            ->name('admin.role.store');
        Route::get('/{id}/edit', [RoleController::class, 'edit'])
            ->name('admin.role.edit');
        Route::post('/{id}/edit', [RoleController::class, 'update'])
            ->name('admin.role.update');
        Route::delete('/{id}/destroy/', [RoleController::class, 'destroy'])
            ->name('admin.role.destroy');
        Route::get('/sync', [RoleController::class, 'sync'])
            ->name('admin.role.sync');
    });

    Route::group([
        'prefix' => 'header'
    ], function () {
        Route::get('/', [HeaderController::class, 'index'])
            ->name('admin.header.index');
        Route::get('/create', [HeaderController::class, 'create'])
            ->name('admin.header.create');
        Route::post('/create', [HeaderController::class, 'store'])
            ->name('admin.header.store');
        Route::get('/{id}/header', [HeaderController::class, 'edit'])
            ->name('admin.header.edit');
        Route::post('/{id}/header', [HeaderController::class, 'update'])
            ->name('admin.header.update');
        Route::delete('/{id}/destroy/', [HeaderController::class, 'destroy'])
            ->name('admin.header.destroy');
    });

    Route::group([
        'prefix' => 'seo'
    ], function () {
        Route::get('/', [SEOController::class, 'index'])
            ->name('admin.seo.index');
        Route::get('/create', [SEOController::class, 'create'])
            ->name('admin.seo.create');
        Route::post('/create', [SEOController::class, 'store'])
            ->name('admin.seo.store');
        Route::get('/{id}/edit', [SEOController::class, 'edit'])
            ->name('admin.seo.edit');
        Route::post('/{id}/edit', [SEOController::class, 'update'])
            ->name('admin.seo.update');
        Route::delete('/{id}/destroy/', [SEOController::class, 'destroy'])
            ->name('admin.seo.destroy');
    });
    Route::group([
        'prefix' => 'notification'
    ], function () {
        Route::get('/', [NotificationController::class, 'index'])
            ->name('admin.notification.index');
        Route::get('/create', [NotificationController::class, 'create'])
            ->name('admin.notification.create');
        Route::post('/create', [NotificationController::class, 'store'])
            ->name('admin.notification.store');
    });
    Route::get('/language/{language}',[LanguageController::class, 'changeLanguage'])->name('language');

    Route::group([
        'prefix' => 'product'
    ], function () {
        Route::get('/', [ProductController::class, 'index'])
            ->name('admin.product.index');
        Route::get('/create', [ProductController::class, 'create'])
            ->name('admin.product.create');
        Route::post('/create', [ProductController::class, 'store'])
            ->name('admin.product.store');
        Route::get('/{id}/edit', [ProductController::class, 'edit'])
            ->name('admin.product.edit');
        Route::post('/{id}/edit', [ProductController::class, 'update'])
            ->name('admin.product.update');
        Route::delete('/{id}/destroy/', [ProductController::class, 'destroy'])
            ->name('admin.product.destroy');
    });
    Route::group([
        'prefix' => 'theme'
    ], function () {
        Route::get('/', [ThemeController::class, 'index'])
            ->name('admin.theme.index');
        Route::get('/create', [ThemeController::class, 'create'])
            ->name('admin.theme.create');
        Route::post('/create', [ThemeController::class, 'store'])
            ->name('admin.theme.store');
        Route::get('/{id}/edit', [ThemeController::class, 'edit'])
            ->name('admin.theme.edit');
        Route::post('/{id}/edit', [ThemeController::class, 'update'])
            ->name('admin.theme.update');
        Route::delete('/{id}/destroy/', [ThemeController::class, 'destroy'])
            ->name('admin.theme.destroy');
    });
});

