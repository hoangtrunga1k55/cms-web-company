<?php
return [
    'admin.category.store' => [
        'title' => 'Thêm Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.category.update' => [
        'title' => 'Sửa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.category.destroy' => [
        'title' => 'Xóa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'admin.new.store' => [
        'title' => 'Thêm Bài Viết',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.new.update' => [
        'title' => 'Sửa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.new.destroy' => [
        'title' => 'Xóa Danh Mục',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'admin.config.store' => [
        'title' => 'Thêm Cấu Hình',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.config.update' => [
        'title' => 'Sửa Cấu Hình',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.config.destroy' => [
        'title' => 'Xóa Cấu Hình',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'admin.cms-account.store' => [
        'title' => 'Thêm Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.cms-account.update' => [
        'title' => 'Sửa Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.cms-account.destroy' => [
        'title' => 'Xóa Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'admin.banner.store' => [
        'title' => 'Thêm Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.banner.update' => [
        'title' => 'Sửa Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.banner.destroy' => [
        'title' => 'Xóa Tài Khoản',
        'extra_name_params' => ['title'],
        'insert_id' => 'id',
        'method' => 'get',
    ],
    'admin.package.store' => [
        'title' => 'Thêm Tài Khoản',
        'extra_name_params' => ['name'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.package.update' => [
        'title' => 'Sửa Tài Khoản',
        'extra_name_params' => ['name'],
        'insert_id' => 'id',
        'method' => 'post',
    ],
    'admin.package.destroy' => [
        'title' => 'Xóa Tài Khoản',
        'extra_name_params' => ['name'],
        'insert_id' => 'id',
        'method' => 'get',
    ]

];
