<?php
return [
    "Dashboard" => [
        'Xem Dashboard' => [
            'admin.dashboard',
        ],
    ],
    "Quản lý đối tác" => [
        'Xem đối tác' => [
            'admin.brand.index',
        ],
        'Thêm đối tác' => [
            'admin.brand.create',
            'admin.brand.store',
        ],
        'Sửa đối tác' => [
            'admin.brand.edit',
            'admin.brand.update',
        ],
        'Xóa đối tác' => [
            'admin.brand.destroy',
        ],
    ],
    "Quản lý gói bán hàng" => [
        'Xem gói bán hàng' => [
            'admin.package.index',
        ],
        'Thêm gói bán hàng' => [
            'admin.package.create',
            'admin.package.store',
        ],
        'Sửa gói bán hàng' => [
            'admin.package.edit',
            'admin.package.update',
        ],
        'Xóa gói bán hàng' => [
            'admin.package.destroy',
        ],
    ],
    "Quản lý banner" => [
        'Xem banner' => [
            'admin.banner.index',
        ],
        'Thêm banner' => [
            'admin.banner.create',
            'admin.banner.store',
        ],
        'Sửa banner' => [
            'admin.banner.edit',
            'admin.banner.update',
        ],
        'Xóa banner' => [
            'admin.banner.destroy',
        ],
    ],
    "Quản lý tin tức" => [
        'Xem tin tức' => [
            'admin.new.index',
        ],
        'Thêm tin tức' => [
            'admin.new.create',
            'admin.new.store',
        ],
        'Sửa tin tức' => [
            'admin.new.edit',
            'admin.new.update',
        ],
        'Quyền duyệt' => [
            'admin.new.approve',
        ],
        'Quyền công bố' => [
            'admin.new.publish',
        ],
        'Xóa tin tức' => [
            'admin.new.destroy',
        ],
        'Cập nhật trạng thái tin tức' => [
            'admin.new.update-status',
        ],
    ],
    "Quản lý danh mục" => [
        'Xem danh mục' => [
            'admin.category.index',
        ],
        'Thêm danh mục' => [
            'admin.category.create',
            'admin.category.store',
        ],
        'Sửa danh mục' => [
            'admin.category.edit',
            'admin.category.update',
        ],
        'Xóa danh mục' => [
            'admin.category.destroy',
        ],
    ],
    "Quản lý Nhà cung cấp" => [
        'Xem Nhà cung cấp' => [
            'admin.vendor.index',
        ],
        'Thêm Nhà cung cấp' => [
            'admin.vendor.create',
            'admin.vendor.store',
        ],
        'Sửa Nhà cung cấp' => [
            'admin.vendor.edit',
            'admin.vendor.update',
        ],
        'Xóa Nhà cung cấp' => [
            'admin.vendor.destroy',
        ],
    ],
    "Quản lý tài khoản" => [
        'Quản lý tài khoản cms' => [
            'admin.cms-account.index',
            'admin.cms-account.create',
            'admin.cms-account.store',
            'admin.cms-account.edit',
            'admin.cms-account.update',
            'admin.cms-account.destroy',
        ]
    ],
    "Quản lý cấu hình" => [
        'Xem cấu hình' => [
            'admin.config.index',
        ],
        'Thêm cấu hình' => [
            'admin.config.create',
            'admin.config.store',
        ],
        'Sửa cấu hình' => [
            'admin.config.edit',
            'admin.config.update',
        ],
        'Xóa cấu hình' => [
            'admin.config.destroy',
        ],
    ],
    "Quản lý quyền" => [
        'Xem quyền' => [
            'admin.permission.index',
        ],
        'Thêm quyền' => [
            'admin.permission.create',
            'admin.permission.store',
        ],
        'Sửa quyền' => [
            'admin.permission.edit',
            'admin.permission.update',
        ],
        'Xóa quyền' => [
            'admin.permission.destroy',
        ],
    ],
    "Quản lý nhóm quyền" => [
        'Xem nhóm quyền' => [
            'admin.role.index',
        ],
        'Thêm nhóm quyền' => [
            'admin.role.create',
            'admin.role.store',
        ],
        'Sửa nhóm quyền' => [
            'admin.role.edit',
            'admin.role.update',
        ],
        'Xóa nhóm quyền' => [
            'admin.role.destroy',
        ],
        'Khởi tạo nhóm quyền' => [
            'admin.role.sync',
        ],
    ],
    "Quản lý tag" => [
        'Xem tag' => [
            'admin.tag.index',
        ],
        'Thêm tag' => [
            'admin.tag.create',
            'admin.tag.store',
        ],
        'Sửa tag' => [
            'admin.tag.edit',
            'admin.tag.update',
        ],
        'Xóa tag' => [
            'admin.tag.destroy',
        ],
    ],
    "Quản lý menu" => [
        'Xem menu' => [
            'admin.header.index',
        ],
        'Thêm menu' => [
            'admin.header.create',
            'admin.header.store',
        ],
        'Sửa menu' => [
            'admin.header.edit',
            'admin.header.update',
        ],
        'Xóa menu' => [
            'admin.header.destroy',
        ],
    ],
];
