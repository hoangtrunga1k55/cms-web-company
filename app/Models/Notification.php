<?php

namespace App\Models;

use App\Repositories\Contracts\MemberRepositoryInterface;
use Illuminate\Support\Facades\Cache;

class Notification extends MongoModel
{
    protected $collection = 'notifications';
    protected $guarded = [];

    public function user(){
        if (!Cache::tags('query')->has('members')) {
            Cache::tags('query')->add('members', app(MemberRepositoryInterface::class)->all(), now()->addMinutes(10));
        }
        $item = Cache::tags('query')->get('members');
        $id = $this->user_id;
        return $item->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();
    }
}
