<?php

namespace App\Models;

class Theme extends MongoModel
{
    //
    protected $guarded = [];

    const STATUS = [
        '1' => "Hiển thị",
        '0' => "Ẩn",
    ];

    public function getStatus()
    {
        if (isset(self::STATUS[$this->status ?? "0"])) {
            return self::STATUS[$this->status ?? "0"];
        }
        return "-";
    }
}
