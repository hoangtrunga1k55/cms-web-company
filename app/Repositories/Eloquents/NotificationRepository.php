<?php

namespace App\Repositories\Eloquents;

use App\Models\Notification;
use App\Repositories\Contracts\NotificationRepositoryInterface;

class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    function __construct(Notification $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Notification::class;
    }
}
