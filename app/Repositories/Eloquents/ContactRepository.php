<?php

namespace App\Repositories\Eloquents;

use App\Models\Contact;
use App\Repositories\Contracts\ContactRepositoryInterface;

class ContactRepository extends BaseRepository implements ContactRepositoryInterface
{
    function __construct(Contact $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Contact::class;
    }
}
