<?php

namespace App\Repositories\Eloquents;

use App\Models\CmsAccount;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;

class CmsAccountRepository extends BaseRepository implements CmsAccountRepositoryInterface
{
    function __construct(CmsAccount $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return CmsAccount::class;
    }
}
