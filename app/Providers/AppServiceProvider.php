<?php

namespace App\Providers;

use Illuminate\Support\Env;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (\env('APP_ENV', 'local') != 'local') {
            URL::forceScheme('https');
        }

        $models = [
            "CmsAccount",
            "Category",
            "New",
            "PasswordReset",
            "Log",
            "Config",
            "Banner",
            "Contact",
            "Role",
            "Permission",
            "Package",
            "Brand",
            "Logo",
            "Tag",
            "Header",
            "Vendor",
            "SEO",
            "Notification",
            "Member",
            "Language",
            "Product",
            "Order",
            "Theme",
        ];

        foreach ($models as $model) {
            $this->app->bind('App\Repositories\Contracts\\' . $model . 'RepositoryInterface', 'App\Repositories\Eloquents\\' . $model . 'Repository');
        }
    }
}
