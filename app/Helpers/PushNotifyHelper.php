<?php

namespace App\Helpers;


use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;

class PushNotifyHelper
{
    public static function pushMutilDevices($devices = [], $title, $body, $data = [])
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

//        $tokens = ['f5YCy0joSiQ:APA91bFlEa6_uwypqoNvXy44u_i6UkgEHEJyofQyB6u5DiybjQwg-cvwWMX9IDyGaRAtoPZRT3u-O4AgJe3Ss_jXAA-BvYkThB8csarmI-NT57sEziqADcaXGnxlx_IryIRgSrc2f7Or'];

        $result = FCM::sendTo($devices, $option, $notification, $data);

        if ($result->numberSuccess() > 0) {
            return ['success' => 1, 'msg' => 'Gửi thông báo thành công'];
        } else {
            return ['success' => 0, 'msg' => 'Gửi thông báo không thành công'];
        }
    }

    public static function pushToTopic($topic_name, $title, $body, $data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($title);
        $notificationBuilder->setBody($body)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $topic = new Topics();

        $topic->topic($topic_name);

        return FCM::sendToTopic($topic, $option, $notification, $data);
        $result = FCM::sendToTopic($topic, $option, $notification, $data);

        if ($result->isSuccess()) {
            return ['success' => 1, 'msg' => 'Gửi thông báo thành công'];
        } else {
            return ['success' => 0, 'msg' => 'Gửi thông báo không thành công'];
        }
    }

}
