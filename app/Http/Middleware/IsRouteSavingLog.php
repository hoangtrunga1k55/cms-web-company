<?php

namespace App\Http\Middleware;

use App\Helpers\LogsHelper;
use App\Repositories\Contracts\LogRepositoryInterface;
use Closure;
use Illuminate\Support\Facades\Config;

class IsRouteSavingLog
{
    protected $logRepository;
    public function __construct(LogRepositoryInterface $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route =  \Request::route()->getName();
        $listes = Config::get('action_log');
        foreach ($listes as $key => $list){
            if ($key == $route){
                $data = LogsHelper::save($request,$list);
                $this->logRepository->create($data);
                break;
            }
        }
            return $next($request);
    }
}
