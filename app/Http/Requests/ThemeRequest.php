<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ThemeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => "required|unique:themes,title,$this->id,_id",
            'price' => 'required',
            'unit' => 'required',
            'completion_date' => 'required',
            'content' => 'required',
            'demo_web' => 'required',
            'demo_android' => 'required',
            'demo_ios' => 'required',
            'image' => 'required',
            'meta_title' => 'max:70',
            'meta_description' => 'max:160',
            'meta_keywords' => '',
            'meta_thumbnail' => '',
            'status' => 'required|in:0,1'
        ];
        return $rules;
    }
}
