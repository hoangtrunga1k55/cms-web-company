<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CmsAccountsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'full_name' => 'required',
            'status' => 'required|in:0,1',
            'roles' => 'required',
            'image' => '',
        ];

        if ($this->id) {
            $rules['email'] = "required|unique:cms_accounts,email,{$this->id},_id";

            return $rules;
        }
        $rules['image'] .= '|required';
        $rules['email'] = "required|unique:cms_accounts";
        $rules ['password'] = 'required|min:8';

        return $rules;
    }
}
