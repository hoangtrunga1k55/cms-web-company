<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateMember extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:255',
            'email' => 'max:255',
            'address' => 'max:255',
            'phone_number' => '',
            'avatar'=>'',
            'password' => '',
            'birth_day' =>''
        ];
        if (!empty($this->id)) {
            return $rules;
        }
        $rules['email'] .= "required|unique:users";
        $rules ['password'] .= 'required|min:8';
        $rules['phone_number'].= 'required|regex:/(01)[0-9]{9}/|unique:users,phone_number,' . $this->id . ',_id|min:10|max:20';
        return  $rules;
    }
}
