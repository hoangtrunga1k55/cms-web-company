<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\InfoProfileRequest;
use App\Http\Requests\PasswordProfileRequest;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    protected $cmsAccountRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
    }

    public function index(Request $request)
    {
        return view('admin.pages.profile.index', [
            'user' => Auth::user()
        ]);
    }

    public function updateInfo(InfoProfileRequest $request)
    {
        $attributes = $request->validated();
        $this->cmsAccountRepository->update($attributes, Auth::id());

        return response()->json([
            'message' => 'Cập nhật thông tin thành công',
            'status' => 'success',
            'url' => route('admin.profile.index')
        ]);
    }

    public function updatePassword(PasswordProfileRequest $request)
    {
        if (Hash::check($request->old_password, Auth::user()->getAuthPassword())) {
            $this->cmsAccountRepository->update([
                'password' => $request->password
            ], Auth::id());

            return response()->json([
                'message' => 'Cập nhật mật khẩu thành công',
                'status' => 'success',
                'url' => route('admin.profile.index')
            ]);
        } else {
            return response()->json([
                'message' => 'Mật khẩu cũ không chính xác',
                'status' => 'error',
                'url' => route('admin.profile.index')
            ]);
        }

    }
}
