<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

class PermissionController extends Controller
{
    protected $permissionRepository;

    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }


    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('permissions')) {
            Cache::tags('query')->add('permissions', $this->permissionRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('permissions');
    }

    public function index()
    {
        $permissions = PaginateHelper::paginate($this->cacheQuery());

        return view('admin.pages.permission.index', compact(
            'permissions'
        ));
    }

    public function create()
    {
        $filteredRoutes = $this->getAllRoutes();

        return view('admin.pages.permission.form', compact(
            'filteredRoutes'
        ));
    }

    function getAllRoutes()
    {
        if (!Cache::tags('query')->has('routes')){
            $routeCollection = Route::getRoutes();
            $arr = [];
            foreach ($routeCollection as $route) {
                if (in_array('permission', $route->action['middleware'] ?? [])) {
                    $arr[] = $route->action['as'];
                }
            }
            Cache::tags('query')->add('routes', array_unique($arr), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('routes');
    }

    public function store(PermissionRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.permission.index')
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();

        if (!is_null($id)) {
            $this->permissionRepository->update($attributes, $id);
        } else {
            $this->permissionRepository->create($attributes);
        }

        Cache::tags('permissions')->flush();

    }

    public function edit($id)
    {
        $filteredRoutes = $this->getAllRoutes();
        $permission = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();

        if (empty($permission)) return redirect()->route('permission.index');

        return view('admin.pages.permission.form', compact(
            'permission',
            'filteredRoutes'
        ));
    }

    public function update(PermissionRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);


        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.permission.index')
        ]);
    }

    public function destroy($id)
    {
        $this->permissionRepository->destroy($id);

        Cache::tags('permissions')->flush();

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}
