<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VendorRequest;
use App\Repositories\Contracts\VendorRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class VendorController extends Controller
{

    protected $vendorRepository;

    public function __construct(VendorRepositoryInterface $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('vendors')) {
            Cache::tags('query')->add('vendors', $this->vendorRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('vendors');
    }
    public function index(Request $request)
    {
        $vendors = $this->cacheQuery();

        if (!is_null($request->email)) {
            $vendors = $vendors->filter(function ($item) use ($request) {
                return $item->status == $request->status;
            });
        }
        return view('admin.pages.vendor.list',compact(
            'vendors','request'
        ));
    }

    public function create()
    {
        return view('admin.pages.vendor.form');
    }


    public function store(VendorRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Tạo thành công',
            'status' => 'success',
            'url' => route('admin.vendor.index')
        ]);
    }

    public function edit($id)
    {
        $vendor = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();
        return view('admin.pages.vendor.form', compact('vendor'));
    }

    public function update(VendorRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.vendor.index')
        ]);
    }


    public function destroy($id)
    {
        $this->vendorRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }


    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];
        if (!is_null($id)) {
            $this->vendorRepository->update($attributes, $request->id);
        } else {
            $this->vendorRepository->create($attributes);
        }

    }
}
