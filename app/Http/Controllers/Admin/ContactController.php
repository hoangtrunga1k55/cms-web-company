<?php

namespace App\Http\Controllers\Admin;
use App\Helpers\PaginateHelper;
use App\Repositories\Contracts\ContactRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ContactController extends Controller
{
    protected $contactRepository;

    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }
    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('contacts')) {
            Cache::tags('query')->add('contacts', $this->contactRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('contacts');
    }

    public function index()
    {
        $contacts= $this->cacheQuery();
        $contacts = PaginateHelper::paginate($contacts);
        return view('admin.pages.contact.list',compact('contacts'));
    }

    public function getInfo(Request $request){
        $contact = $this->contactRepository->findById($request->id);
        $contact->message = strip_tags($contact->message);
        return response()->json([
           'contact' => $contact
        ]);
    }
}
