<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\HeaderRequest;
use App\Repositories\Contracts\HeaderRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class HeaderController extends Controller
{
    protected $headerRepository;
    protected $languageRepository;

    public function __construct(HeaderRepositoryInterface $headerRepository, LanguageRepositoryInterface $languageRepository)
    {
        $this->headerRepository = $headerRepository;
        $this->languageRepository = $languageRepository;
    }

    public function cacheQuery()
    {
        $condition = [];
        $condition['where'][] = ['lang_type', 'like', Config::get('app.locale')];
        if (!Cache::tags('query')->has('headers')) {
            Cache::tags('query')->add('headers', $this->headerRepository->all(['*'],$condition), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('headers');
    }


    public function index()
    {

        $headers =  $this->cacheQuery();
        $headers = PaginateHelper::paginate($headers);
        return view('admin.pages.header.index', compact(
            'headers'
        ));
    }

    public function create()
    {
        $languages = $this->languageRepository->all();
        return view('admin.pages.header.form', compact('languages'));
    }

    public function store(HeaderRequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.header.index')
        ]);
    }

    public function edit($id)
    {
        $languages = $this->languageRepository->all();
        $header = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();
        if (empty($header)) return redirect()->route('admin.header.index');

        return view('admin.pages.header.form', compact(
            'header', 'languages'
        ));
    }

    public function update(HeaderRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);


        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.header.index')
        ]);
    }

    public function destroy($id)
    {
        $this->headerRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->headerRepository->update($attributes, $id);
        } else {
            $this->headerRepository->create($attributes);
        }

    }
}
