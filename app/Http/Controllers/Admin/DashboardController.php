<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\NewRepositoryInterface;
use App\Repositories\Contracts\NotificationRepositoryInterface;
use App\Repositories\Contracts\ContactRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    protected $cmsAccountRepository;
    protected $memberRepository;
    protected $newRepository;
    protected $notificationRepository;
    protected $contactRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository,
                                MemberRepositoryInterface $memberRepository,
                                NotificationRepositoryInterface $notificationRepository,
                                ContactRepositoryInterface $contactRepository,
                                NewRepositoryInterface $newRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->contactRepository = $contactRepository;
        $this->memberRepository = $memberRepository;
        $this->newRepository = $newRepository;
        $this->notificationRepository = $notificationRepository;
    }

    public function index()
    {
        $countUser = $this->cmsAccountRepository->count();
        $countCustomer = $this->contactRepository->count();
        $countNew = $this->newRepository->count();
        $orderBy = array('created_at' => 'asc');
        $condition['where'][] = ['created_at', '>=', now()->firstOfMonth()];
        $nru = $this->memberRepository->all(array('*'), $condition, [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        $articles = $this->newRepository->all(array('*'), $condition, [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        $notify = $this->notificationRepository->all(array('*'), $condition, [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        return view('admin.pages.dashboard', compact(
            'countUser', 'countCustomer', 'countNew', 'nru', 'articles', 'notify'
        ));
    }
}
