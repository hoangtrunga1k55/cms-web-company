<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\LanguageRequest;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LanguageController extends Controller
{

    protected $languageRepository;

    public function __construct(LanguageRepositoryInterface $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('languages')) {
            Cache::tags('query')->add('languages', $this->languageRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('languages');
    }

    public function getLang()
    {
        $languages = $this->languageRepository->all();
        foreach ($languages as $language) {
            if ($language['code'] == Config::get('app.locale')) {
                $path_lang = resource_path() . '/lang/' . $language['code'] . '.json';
                $lang = file_get_contents($path_lang);
            }
        }
        $lang = json_decode($lang, true);
        $language = Config::get('app.locale');
        return view('admin.pages.language.lang_config', compact('lang', 'language'));
    }

    public function getConfigLanguage(Request $request)
    {
        $lang = $request->lang;
        $languages = $this->languageRepository->all();
        foreach ($languages as $language) {
            if ($language['code'] == $lang) {
                $path_lang = public_path($language['code'] . '.json');
                $data = file_get_contents($path_lang);
            }
        }
        return $data;
    }

    public function save(Request $request)
    {
        $languages = $this->languageRepository->all();
        $data = $request->all();
        $config_langs = $data['config_languages'];
        $code = $data['code'];
        $lang = "";
        foreach ($config_langs as $config_lang) {
            $lang .= '"' . $config_lang["key"] . '":"' . $config_lang["value"] . '",';
        }
        $lang_end = "{" . $lang;
        $pos = strrpos($lang_end, ',');
        if ($pos !== false) {
            $lang_end = substr_replace($lang_end, '', $pos, strlen($lang_end));
        }
        $lang_end .= "}";
        foreach ($languages as $language) {
            if ($language['code'] == $code) {
                $path_lang = resource_path() . '/lang/' . $language['code'] . '.json';
                file_put_contents($path_lang, $lang_end);
                file_put_contents(public_path($language['code'] . '.json'), $lang_end);
            }
        }
        return $data;
    }

    public function index(Request $request)
    {
        $languages = $this->cacheQuery();
        $languages = $languages->filter(function ($item) use ($request) {
            return !!preg_match("/" . $request->status . "/", $item->status);
        });

        $languages = PaginateHelper::paginate($languages);
        return view('admin.pages.language.list', compact(
            'languages', 'request'
        ));
    }

    public function create()
    {
        return view('admin.pages.language.form');
    }


    public function store(LanguageRequest $request)
    {
        $this->handleSubmitRequest($request);
        $attributes = $request->validated();
        $lang_file = $attributes['code'] . '.json';
        if (!file_exists($lang_file)) {
            touch(resource_path() . '/lang/' . $lang_file);
            \File::copy(resource_path() . '/lang/vn.json', resource_path() . '/lang/' . $lang_file);
        }
        return response()->json([
            'message' => 'Tạo thành công',
            'status' => 'success',
            'url' => route('admin.language.index')
        ]);
    }

    public function edit($id)
    {
        $language =  $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();
        return view('admin.pages.language.form', compact('language'));
    }

    public function update(LanguageRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.language.index')
        ]);
    }


    public function destroy($id)
    {
        $this->languageRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function changeLanguage($language)
    {
        \Illuminate\Support\Facades\Session::put('language', $language);
        return redirect()->back();
    }

    public function changeLanguageWeb($language)
    {
        \Illuminate\Support\Facades\Session::put('language', $language);
        return redirect()->route('site.index');
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];
        $attributes['default'] = (int)$attributes['default'];
        if ($attributes['default'] === 1) {
            $affected = DB::table('languages')->where('default', '=', 1)->update(array('default' => 0));
            config(['app.locale' => $attributes['code']]);
        }
        if (!is_null($id)) {
            $this->languageRepository->update($attributes, $request->id);
        } else {
            $this->languageRepository->create($attributes);
        }
    }
}
