<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\AutoIncrementId;
use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\ProductRepositoryInterface;
use Illuminate\Support\Facades\Cache;

class ProductController extends Controller
{
    protected $productRepository;
    protected $categoryRepository;

    public function __construct(ProductRepositoryInterface $productRepository,
                                CategoryRepositoryInterface $categoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $products = PaginateHelper::paginate($this->cacheQuery());

        return view('admin.pages.product.index', compact(
            'products'
        ));
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('packages')) {
            Cache::tags('query')->set('packages', $this->productRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('packages');
    }

    public function create()
    {
        $categories = $this->cacheCategoriesQuery()->filter(function ($item) {
            return $item->type == 'product';
        });
        return view('admin.pages.product.form', compact('categories'));
    }

    public function cacheCategoriesQuery()
    {
        if (!Cache::tags('query')->has('categories')) {
            Cache::tags('query')->set('categories', $this->productRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('categories');
    }

    public function store(ProductRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.product.index')
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->productRepository->update($attributes, $request->id);
        } else {
            $attributes['order_id'] = now()->format('Ymd') . str_pad(AutoIncrementId::getId('order'), 6, 0);
            $this->productRepository->create($attributes);
        }
    }

    public function edit($id)
    {
        $product = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();

        $categories = $this->cacheCategoriesQuery()->filter(function ($item) {
            return $item->type == 'product';
        });

        if (empty($product)) return redirect()->route('admin.product.index');

        return view('admin.pages.product.form', compact(
            'product', 'categories'
        ));
    }

    public function update(ProductRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);
        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.product.index')
        ]);
    }

    public function destroy($id)
    {
        $this->productRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}
