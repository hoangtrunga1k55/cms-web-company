<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Requests\ConfigRequest;
use App\Repositories\Contracts\ConfigRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class ConfigController extends Controller
{
    protected $configRepository;
    protected $languageRepository;

    public function __construct(ConfigRepositoryInterface $configRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->configRepository = $configRepository;
        $this->languageRepository = $languageRepository;
    }

    public function cacheQuery()
    {
        $condition = [];
        $condition['where'][] = ['lang_type', 'like', Config::get('app.locale')];
        if (!Cache::tags('query')->has('configs')) {
            Cache::tags('query')->add('configs', $this->configRepository->all(['*'],$condition), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('configs');
    }

    public function index(Request $request)
    {
        $configs = $this->cacheQuery();
        $configs = $configs->filter(function ($item) use ($request) {
            return !!preg_match("/" . $request->code . "/", $item->lang_type);
        });
        $configs = $configs->filter(function ($item) use ($request) {
            return !!preg_match("/" . $request->key . "/", $item->key);
        });
        $configs = PaginateHelper::paginate($configs);
        $languages = $this->languageRepository->all();
        return view('admin.pages.config.list', compact('configs', 'request','languages'));
    }

    public function create()
    {
        $languages = $this->languageRepository->all();
        return view('admin.pages.config.form',compact('languages'));
    }

    public function store(ConfigRequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.config.index')
        ]);
    }

    public function edit($id)
    {
        $config = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();
        $languages = $this->languageRepository->all();
        return view('admin.pages.config.form', compact('config','languages'));
    }

    public function update(ConfigRequest $request, $id)
    {

        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.config.index')
        ]);
    }

    public function destroy($id)
    {
        $this->configRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->configRepository->update($attributes, $id);
        } else {
            $this->configRepository->create($attributes);
        }

    }
}
