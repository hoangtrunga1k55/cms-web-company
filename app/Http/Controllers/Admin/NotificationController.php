<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Helpers\PushNotifyHelper;
use App\Http\Requests\NotifyRequest;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\NotificationRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class NotificationController extends Controller
{
    protected $notificationRepository;
    protected $memberRepository;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        MemberRepositoryInterface $memberRepository
    )
    {
        $this->notificationRepository = $notificationRepository;
        $this->memberRepository = $memberRepository;
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('notifications')) {
            Cache::tags('query')->add('notifications', $this->notificationRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('notifications');
    }

    public function cacheMembersQuery()
    {
        if (!Cache::tags('query')->has('members')) {
            Cache::tags('query')->add('members', $this->memberRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('members');
    }

    public function index(Request $request)
    {
        $notifications = $this->cacheQuery();

        if (!is_null($request->title)) {
            $notifications = $notifications->filter(function ($item) use ($request) {
                return !!preg_match("/" . $request->title . "/", $item->title);
            });
        }
        if (!is_null($request->key)) {
            $notifications = $notifications->filter(function ($item) use ($request) {
                return !!preg_match("/" . $request->key . "/", $item->key);
            });
        }

        $notifications = PaginateHelper::paginate($notifications);
        return view('admin.pages.notification.list', compact(
            'notifications',
            'request'
        ));
    }

    public function create()
    {
        $users = $this->cacheMembersQuery();

        return view('admin.pages.notification.form', compact('users'));
    }

    public function store(NotifyRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm mới thành công',
            'status' => 'success',
            'url' => route('admin.notification.index')
        ]);
    }


    public function destroy($id)
    {
        $this->notificationRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request)
    {
        $attributes = [];
        $attributes['title'] = $request->title;
        $attributes['body'] = $request->body;
        $attributes['type'] = $request->type;

        $attributes['data'] = [
            'type' => "topic",
        ];
        if ($attributes['type'] === "topic") {
            $attributes['topic'] = $request->topic;
            $result = PushNotifyHelper::pushToTopic(
                $attributes['topic'],
                $attributes['title'],
                $attributes['body'],
                $attributes['data']
            );
            $attributes['status'] = $result->isSuccess();
            $attributes['sender'] = Auth::id();
            $this->notificationRepository->create($attributes);
        } else {
            foreach ($request->user_ids as $user_id) {
                $attributes['user_id'] = $user_id;
                $result = PushNotifyHelper::pushToTopic(
                    $attributes['user_id'],
                    $attributes['title'],
                    $attributes['body'],
                    $attributes['data']
                );
                $attributes['status'] = $result->isSuccess();
                $attributes['sender'] = Auth::id();
                $this->notificationRepository->create($attributes);
            }
        }
    }
}
