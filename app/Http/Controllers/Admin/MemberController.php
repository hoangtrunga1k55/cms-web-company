<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMember;
use App\Repositories\Contracts\MemberRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MemberController extends Controller
{
    protected $memberRepository;

    public function __construct(MemberRepositoryInterface $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('members')) {
            Cache::tags('query')->add('members', $this->memberRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('members');
    }

    public function index(Request $request)
    {
        $members = $this->cacheQuery();

        if (!is_null($request->email)) {
            $members = $members->filter(function ($item) use ($request) {
                return !!preg_match("/" . $request->email . "/", $item->email);
            });
        }
        if (!is_null($request->phone_number)) {
            $members = $members->filter(function ($item) use ($request) {
                return !!preg_match("/" . $request->phone_number . "/", $item->phone_number);
            });
        }
        if (!is_null($request->address)) {
            $members = $members->filter(function ($item) use ($request) {
                return !!preg_match("/" . $request->address . "/", $item->address);
            });
        }

        $members = PaginateHelper::paginate($members);
        return view('admin.pages.member.index', compact(
            'members',
            'request'
        ));
    }

    public function edit($id)
    {
        $member = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();

        if (empty($member)) return redirect()->route('members.index');

        return view('admin.pages.member.form', compact(
            'member'
        ));
    }

    public function update(UpdateMember $request, $id)
    {
        $attributes = $request->validated();
        if (!is_null($request->avatar)) {
            $attributes['avatar'] = $request->avatar;
        } else {
            unset($attributes['avatar']);
        }
        if (!is_null($request->password)) {
            $attributes['password'] = bcrypt($request->password);
        } else {
            unset($attributes['password']);
        }
        if (isset($attributes['birth_day'])) {
            $attributes['birth_day'] = (int)Carbon::createFromFormat('Y-m-d', $request->birth_day)
                ->timestamp;
        }
        $this->memberRepository->update($attributes, $id);
        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.member.index')
        ]);
    }

    public function destroy($id)
    {
        $this->memberRepository->destroy($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}
