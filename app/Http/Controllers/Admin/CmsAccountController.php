<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CmsAccountsRequest;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use Illuminate\Http\Request;
use App\Helpers\ImageHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class CmsAccountController extends Controller
{
    protected $cmsAccountRepository;
    protected $roleRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository, RoleRepositoryInterface $roleRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->roleRepository = $roleRepository;
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('cms-accounts')) {
            Cache::tags('query')->add('accounts', $this->cmsAccountRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('accounts');
    }

    public function index(Request $request)
    {
        $cmsAccounts = $this->cacheQuery();
        $cmsAccounts = $cmsAccounts->filter(function ($item) use ($request) {
            return !!preg_match("/" . $request->status . "/", $item->status);
        });
        $cmsAccounts = $cmsAccounts->filter(function ($item) use ($request) {
            return !!preg_match("/" . $request->email . "/", $item->email);
        });
        $cmsAccounts = $cmsAccounts->filter(function ($item) use ($request) {
            return !!preg_match("/" . $request->name . "/", $item->name);
        });
        $cmsAccounts = PaginateHelper::paginate($cmsAccounts);
        return view('admin.pages.cms-account.index', compact(
            'cmsAccounts',
            'request'
        ));
    }

    public function create()
    {
        $roles = $this->roleRepository->all();
        return view('admin.pages.cms-account.form', compact(
            'roles'
        ));
    }

    public function store(CmsAccountsRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm mới thành công',
            'status' => 'success',
            'url' => route('admin.cms-account.index')
        ]);
    }

    public function edit($id)
    {
        $cmsAccount = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();
        $roles = $this->roleRepository->all();

        if (empty($cmsAccount)) return redirect()
            ->route('admin.cms_account.index');

        return view('admin.pages.cms-account.form', compact(
            'cmsAccount',
            'roles'
        ));
    }

    public function update(CmsAccountsRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Cập nhật thành công',
            'status' => 'success',
            'url' => route('admin.cms-account.index')
        ]);
    }

    public function destroy($id)
    {
        $this->cmsAccountRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int) $attributes['status'];
        unset($attributes['roles']);
        if (!is_null($id)) {
            $this->cmsAccountRepository
                ->update($attributes, $request->id);
            $this->cmsAccountRepository
                ->findById($request->id)
                ->roles()
                ->sync($request->roles);
        }else{
            $attributes['password'] = bcrypt($request->password);

            $this->cmsAccountRepository
                ->create($attributes)
                ->roles()
                ->sync($request->roles);
        }

        Cache::tags('permissions')->forget($request->id);
    }
}
