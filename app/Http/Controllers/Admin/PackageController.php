<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\Repositories\Contracts\PackageRepositoryInterface;
use Illuminate\Support\Facades\Cache;

class PackageController extends Controller
{
    protected $packageRepository;

    public function __construct(PackageRepositoryInterface $packageRepository)
    {
        $this->packageRepository = $packageRepository;
    }

    public function index()
    {
        $packages = PaginateHelper::paginate($this->cacheQuery());

        return view('admin.pages.package.index', compact(
            'packages'
        ));
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('packages')) {
            Cache::tags('query')->add('packages', $this->packageRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('packages');
    }

    public function create()
    {
        return view('admin.pages.package.form');
    }

    public function store(PackageRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.package.index')
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->packageRepository->update($attributes, $id);
        } else {
            $this->packageRepository->create($attributes);
        }

    }

    public function edit($id)
    {
        $package = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();

        if (empty($package)) return redirect()->route('admin.package.index');

        return view('admin.pages.package.form', compact(
            'package'
        ));
    }

    public function update(PackageRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.package.index')
        ]);
    }

    public function destroy($id)
    {
        $this->packageRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}
