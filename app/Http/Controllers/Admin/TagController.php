<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Repositories\Contracts\TagRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class TagController extends Controller
{
    protected $tagRepository;

    public function __construct(TagRepositoryInterface $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('tags')) {
            Cache::tags('query')->add('tags', $this->tagRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('tags');
    }


    public function index()
    {
        $tags = PaginateHelper::paginate($this->cacheQuery());

        return view('admin.pages.tag.index', compact(
            'tags'
        ));
    }

    public function create()
    {
        return view('admin.pages.tag.form');
    }

    public function store(TagRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.tag.index')
        ]);
    }

    public function edit($id)
    {
        $tag = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();

        if (empty($tag)) return redirect()->route('admin.tag.index');

        return view('admin.pages.tag.form', compact(
            'tag'
        ));
    }

    public function update(TagRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);


        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.tag.index')
        ]);
    }

    public function destroy($id)
    {
        $this->tagRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['slug'] = Str::slug($attributes['name']);
        $attributes['status'] = (int) $attributes['status'];
        if (!is_null($id)) {
            $this->tagRepository->update($attributes, $request->id);
        } else {
            $this->tagRepository->create($attributes);
        }
    }
}
