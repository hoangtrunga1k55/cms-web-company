<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PaginateHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ThemeRequest;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use App\Repositories\Contracts\ThemeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ThemeController extends Controller
{
    protected $themeRepository;
    protected $languageRepository;

    public function __construct(ThemeRepositoryInterface $themeRepository,
                                LanguageRepositoryInterface $languageRepository)
    {
        $this->themeRepository = $themeRepository;
        $this->languageRepository = $languageRepository;
    }


    public function cacheQuery()
    {
        if (!Cache::tags('query')->has('themes')) {
            Cache::tags('query')->add('themes', $this->themeRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('themes');
    }

    public function cacheLanguagesQuery()
    {
        if (!Cache::tags('query')->has('languages')) {
            Cache::tags('query')->add('languages', $this->languageRepository->all(), now()->addMinutes(10));
        }
        return Cache::tags('query')->get('languages');
    }
    public function index(Request $request)
    {
        $themes = PaginateHelper::paginate($this->cacheQuery());
        return view('admin.pages.theme.list', compact(
            'themes',
            'request'
        ));
    }

    public function create()
    {
        $languages = $this->cacheLanguagesQuery();
        return view('admin.pages.theme.form', compact('languages'));
    }

    public function store(ThemeRequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.theme.index')
        ]);
    }

    public function edit($id)
    {
        $theme = $this->cacheQuery()->filter(function ($item) use ($id) {
            return $item->id == $id;
        })->first();

        $languages = $this->cacheLanguagesQuery();

        if (empty($theme)) return redirect()->route('admin.theme.index');

        return view('admin.pages.theme.form', compact(
            'theme',
            'languages'
        ));
    }

    public function update(ThemeRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.theme.index')
        ]);
    }

    public function destroy($id)
    {
        $this->themeRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['slug'] = str_slug($attributes['title'], '-');

        if (!is_null($id)) {
            $this->themeRepository->update($attributes, $request->id);
        } else {
            $this->themeRepository->create($attributes);
        }
    }
}
